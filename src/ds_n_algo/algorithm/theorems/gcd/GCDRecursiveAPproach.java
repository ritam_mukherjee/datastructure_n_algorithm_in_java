package ds_n_algo.algorithm.theorems.gcd;

public class GCDRecursiveAPproach {
    public static void main(String[] args) {

        //Lets take two numbers 55 and 121 and find their GCD
        int num1 = 55, num2 = 121, gcd = 1;

        System.out.println("GCD is:"+ gcd(num2,num1));

    }


    /*Function to get gcd of a and b*/
    private static int gcd(int a, int b)
    {
        if (b == 0)
            return a;
        else
            return gcd(b, a % b);
    }

}
