package ds_n_algo.algorithm.sorting.insertion;

/**
 * @apiNote : this sorting in adaptive nature [ early break ]
 * @see : https://gitlab.com/ritam_mukherjee/datastructure_n_algorithm_in_java/-/blob/master/src/ds_n_algo/algorithm/sorting/insertion/InsertionSortApproach2.java
 */
public class InsertionSortApproach2 {


    public static int[] doInsertionSorting(int[] arr) {

        OuterLoop: /*It maintains regular element (n) traversal of array*/
        for (int i = 0; i < arr.length -1; i++) {

            InnerLoop:/*its maintain backward traversal*/
            for (int j = i + 1; j > 0; j--) {

                SwapMinimum:/*chose two elements and swap lower one to left side*/
                if (arr[j] < arr[j - 1]) {
                    int temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
                else
                    break ;
            }
        }

        return arr;
    }

    public static void main(String[] args) {
        int[] inPut = {5, 8, 2, 6, 9, 7, 4, 0, 1, 2, 3};
        int[] outPut = doInsertionSorting(inPut);
        for (int i = 0; i < outPut.length; i++) {
            System.out.print(outPut[i] + (i != outPut.length - 1 ? ", " : ""));
        }
    }
}
