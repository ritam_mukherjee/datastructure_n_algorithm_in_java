package ds_n_algo.algorithm.sorting.bubble;

public class BubbleSortEarlyBreak {

    public static int[] doBubbleSortingEarlyBreak(int[] arr) {

        OuterLoop: /*It maintains regular element (n) traversal of array*/
        for (int i = 0; i < arr.length; i++) {
            boolean swap=false;
            InnerLoop:/*Maintain another traversal to 1th location to unsortrd location (n-i)*/
            for (int j = arr.length - 1; j >i ; j--) {

                SwapMinimum:/*chose two elements and swap lower one to left side*/
                if (arr[j ] < arr[j -1]) {
                    //swapping
                    int temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                    //change the flag
                    swap=true;
                }
            }
            /*early Break*/
            if(!swap)
                break OuterLoop;
        }
        return arr;
    }
    public static void main(String[] args) {
        int[] inPut = {5, 8, 2, 6, 9, 7, 4, 0, 1, 2, 3};
        int[] outPut = doBubbleSortingEarlyBreak(inPut);
        for (int i = 0; i < outPut.length; i++) {
            System.out.print(outPut[i] + (i != outPut.length - 1 ? ", " : ""));
        }
    }
}
