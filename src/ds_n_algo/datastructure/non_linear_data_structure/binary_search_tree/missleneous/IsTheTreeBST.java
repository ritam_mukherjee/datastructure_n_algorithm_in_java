package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.missleneous;

public class IsTheTreeBST<X extends Comparable> {

    BinarySearchTree binarySearchTree;

    public BinarySearchTree getBinarySearchTree() {
        return binarySearchTree;
    }

    public void setBinarySearchTree(BinarySearchTree binarySearchTree) {
        this.binarySearchTree = binarySearchTree;
    }

    /**
     * @implSpec The method will not work if 0 is an item of tree
     * @param node
     * @param min
     * @param max
     * @return
     */
    public boolean isTheTreeBST(Node<X> node,X min, X max){

        if(node==null)
            return true;

        /*If any node lies outside the Range then BST constraint has been violated*/
        if(node.getItem().compareTo(min)<=0 ||node.getItem().compareTo(max)>0)
            return false;

        /*check whether nodes left  sub-tree and right sub-tree separately are BST or not
                If that, then as a whole the tree is BST*/
        return isTheTreeBST(node.getLeft(),min,node.getItem()) && isTheTreeBST(node.getRight(),node.getItem(),max);
    }


    public static void main(String[] args) {
        BinarySearchTree<Integer> integerTree=new BinarySearchTree<>();

        Node root=new Node(6);
        root.setLeft(new Node(2));
        root.setRight( new Node(10));
        root.getLeft().setLeft(new Node(1));
        root.getLeft().setRight(new Node(4));
        root.getRight().setLeft(new Node(8));
        root.getRight().setRight(new Node(12));

        IsTheTreeBST program=new IsTheTreeBST();
        program.setBinarySearchTree(integerTree);

        System.out.println(program.isTheTreeBST(root,0,12));
    }
}
