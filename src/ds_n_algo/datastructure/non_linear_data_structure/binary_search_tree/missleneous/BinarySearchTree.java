package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.missleneous;

class BinarySearchTree<X extends Comparable>{
    Node Root;

    public Node getRoot() {
        return Root;
    }

    public void setRoot(Node root) {
        Root = root;
    }
  /*  *//**
     * Exclusive  not include high and low items, if it present inside tree
     * @param node
     * @param low
     * @param high
     *//*
    public void printRangeExclusive(Node<X> node,X low,X high){
        if(node==null)
            return;

        if(low.compareTo(node.getItem())<0)
            printRangeExclusive(node.getLeft(),low,high);

        if(low.compareTo(node.getItem())<0 && high.compareTo(node.getItem())>0)
            System.out.println(node.getItem());

        if(high.compareTo(node.getItem())>0)
            printRangeExclusive(node.getRight(),low,high);
    }

    *//**
     * Inclusive include high and low items, if it present inside tree
     * @param node
     * @param low
     * @param high
     *//*
    public void printRangeInclusive(Node<X> node, X low, X high){
        if(node==null)
            return;
        *//*include left subtree if this condition satisfies*//*
        if(low.compareTo(node.getItem())<=0)
            printRangeInclusive(node.getLeft(),low,high);

        if(low.compareTo(node.getItem())<=0 && high.compareTo(node.getItem())>=0)
            System.out.println(node.getItem());

        *//*include right subtree if this condition satisfies*//*
        if(high.compareTo(node.getItem())>=0)
            printRangeInclusive(node.getRight(),low,high);
    }*/
}
