package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.missleneous;


public class PrintNodesInGivenRange<X extends Comparable> {

    BinarySearchTree binarySearchTree;

    public BinarySearchTree getBinarySearchTree() {
        return binarySearchTree;
    }

    public void setBinarySearchTree(BinarySearchTree binarySearchTree) {
        this.binarySearchTree = binarySearchTree;
    }

    /**
     * Exclusive  not include high and low items, if it present inside tree
     * @param node
     * @param low
     * @param high
     */
    public void printRangeExclusive(Node<X> node,X low,X high){
        if(node==null)
            return;

        if(low.compareTo(node.getItem())<0)
            printRangeExclusive(node.getLeft(),low,high);

        if(low.compareTo(node.getItem())<0 && high.compareTo(node.getItem())>0)
            System.out.println(node.getItem());

        if(high.compareTo(node.getItem())>0)
            printRangeExclusive(node.getRight(),low,high);
    }

    /**
     * Inclusive include high and low items, if it present inside tree
     * @param node
     * @param low
     * @param high
     */
    public void printRangeInclusive(Node<X> node, X low, X high){
        if(node==null)
            return;
        /*include left subtree if this condition satisfies*/
        if(low.compareTo(node.getItem())<=0)
            printRangeInclusive(node.getLeft(),low,high);

        if(low.compareTo(node.getItem())<=0 && high.compareTo(node.getItem())>=0)
            System.out.println(node.getItem());

        /*include right subtree if this condition satisfies*/
        if(high.compareTo(node.getItem())>=0)
            printRangeInclusive(node.getRight(),low,high);
    }

    public static void main(String[] args) {
         BinarySearchTree<Integer> integerTree=new BinarySearchTree<>();

        Node root=new Node(6);
        root.setLeft(new Node(2));
        root.setRight( new Node(10));
        root.getLeft().setLeft(new Node(0));
        root.getLeft().setRight(new Node(4));
        root.getRight().setLeft(new Node(8));
        root.getRight().setRight(new Node(12));

        PrintNodesInGivenRange program=new PrintNodesInGivenRange();
        program.setBinarySearchTree(integerTree);

        System.out.println("Integer Tree: Exclusive Items are :" );
        program.printRangeExclusive(root,2,8);
        System.out.println("Integer Tree: Inclusive Items are :" );
        program.printRangeInclusive(root,2,8);

    }

}
