package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.check_identical;

class Node {
    int value;
    Node left;
    Node right;

    Node(int vaule) {
        this.value = vaule;
        left = right = null;
    }
}

public class BinaryTreeCheckIdentical {

    public boolean checkIdentical(Node a, Node b) {

        /* both empty*/
        if (a == null && b == null)
            return true;

        /* both nonempty*/
        if (a != null && b != null)
            return (a.value == b.value)
                    && checkIdentical(a.left, b.left)
                    && checkIdentical(a.right, b.right);


        /* one empty and one  not*/
        return false;

    }

    public static void main(String[] args) {
        BinaryTreeCheckIdentical tree = new BinaryTreeCheckIdentical();

        Node nodeA = new Node(1);
        nodeA.left = new Node(2);
        nodeA.left.left = new Node(4);
        nodeA.left.right = new Node(5);

        Node nodeB = new Node(1);
        nodeB.left = new Node(2);
        nodeB.right = new Node(3);
        nodeB.left.left = new Node(4);
        nodeB.left.right = new Node(5);

        if (tree.checkIdentical(nodeA, nodeB))
            System.out.println("Both trees are identical");
        else
            System.out.println("Trees are not identical");
    }
}
