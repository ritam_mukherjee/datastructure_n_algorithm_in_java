package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.mirror_image.simple;

import java.util.LinkedList;
import java.util.Queue;

class Node{
    int value;
    Node left;
    Node right;

    Node(int vaule){
        this.value=vaule;
        left=right=null;
    }
}
public class BinaryTreeConvertMirror {

    public Node convertMirrorRecursively(Node node){
        if(node == null)
            return node;

        convertMirrorRecursively(node.left);
        convertMirrorRecursively(node.right);

        Node temp=node.left;
        node.left=node.right;
        node.right=temp;

        return node;
    }

    public Node convertMirrorIteratively(Node node){

        if(node==null)
            return node;

        Queue<Node> queue=new LinkedList<>();
        queue.add(node);

        while (queue.size()>0){

            Node current=queue.peek();
            queue.remove();

            Node temp=current.left;
            current.left=current.right;
            current.right=temp;

            if(current.left!=null)
                queue.add(current.left);
            if(current.right!=null)
                queue.add(current.right);

    }
    return node;
    }

    void inOrder(Node node)
    {
        if (node == null)
            return;

        inOrder(node.left);
        System.out.print(node.value + " ");

        inOrder(node.right);
    }

    /* testing for example nodes */
    public static void main(String args[])
    {
        /* creating a binary tree and entering the nodes */
        BinaryTreeConvertMirror tree = new BinaryTreeConvertMirror();
        Node node = new Node(1);
        node.left = new Node(2);
        node.right = new Node(3);
        node.left.left = new Node(4);
        node.left.right = new Node(5);

        /* print inorder traversal of the input tree */
        System.out.println("Inorder traversal of input tree is :");
        tree.inOrder(node);
        System.out.println("");

        /* convert tree to its mirror */
        Node after_node=tree.convertMirrorRecursively(node);

        /* print inorder traversal of the minor tree */

        tree.inOrder(after_node);

        System.out.println("-----------------------");
        Node after_node2=tree.convertMirrorIteratively(node);
        tree.convertMirrorIteratively(after_node2);
        tree.inOrder(after_node2);
    }
}
