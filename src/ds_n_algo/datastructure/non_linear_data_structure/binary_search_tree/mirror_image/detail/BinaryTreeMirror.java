package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.mirror_image.detail;
class Node<T extends Comparable> {
    /*It represents the value of node,which instantiate at the time of node creation*/
    private T item;
    /*All node having two child node LEFT & RIGHT*/
    private Node left;
    private Node right;

    /*Node also have a reference of PARENT node*/
    private Node parent;

    /*Constructor:
        1.Use to specify item/data at the time of creation
        2.Also use initialize all pointer's*/
    public Node(T item) {
        this.item = item;
        this.left = null;
        this.right = null;
        this.parent = null;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}


class BinaryTree<T extends Comparable> {
       Node Root;

    public Node getRoot() {
        return Root;
    }

    public void setRoot(Node root) {
        Root = root;
    }

        /**
     * INORDER traversal
     * @apiNote LEFT-NODE - ROOT-NODE - RIGHT-NODE
     * @param node  : the node to be inserted
     */
    public void print_in_order(Node node) {

        if (node == null) //break-point
            return;
        left:
        if (node.getLeft() != null) {
            //node=node.getLeft();
            print_in_order(node.getLeft());
        }
        root:
        System.out.print(node.getItem() + "<->");
        right:
        if (node.getRight() != null) {
            //node = node.getRight();
            print_in_order(node.getRight());
        }
    }

    /**
     * This method swap between left and right child of each node
     * @param node
     */
    public void mirror(Node<T> node){
        if(node==null)
            return;

        mirror(node.getLeft());
        mirror(node.getRight());

        Node<T> temp=node.getLeft();
        node.setLeft(node.getRight());
        node.setRight(temp);
    }
}

public class BinaryTreeMirror{
     public static void main(String[] args) {
        BinaryTree<Integer> integerTree=new BinaryTree<>();

        Node root=new Node(6);
        root.setLeft(new Node(2));
        root.setRight( new Node(10));
        root.getLeft().setLeft(new Node(0));
        root.getLeft().setRight(new Node(4));
        root.getRight().setLeft(new Node(8));
        root.getRight().setRight(new Node(12));


         BinaryTree<String> stringTree=new BinaryTree<>();
        Node string_root=new Node("ritam");
        string_root.setLeft(new Node("poulami"));
        string_root.setRight( new Node("somdatta"));
        string_root.getLeft().setLeft(new Node("astha"));
        string_root.getLeft().setRight(new Node("ramisha"));
        string_root.getRight().setLeft(new Node("Shalini"));
        string_root.getRight().setRight(new Node("Tulika"));

         System.out.println("Original integer tree is:");
         integerTree.print_in_order(root);
         integerTree.mirror(root);
         System.out.println("\nMirror image of integer tree is:");
         integerTree.print_in_order(root);

         System.out.println("---------------------------------------");

         System.out.println("Original String tree is:");
         stringTree.print_in_order(string_root);
         stringTree.mirror(string_root);
         System.out.println("\nMirror image of String tree is:");
         integerTree.print_in_order(string_root);

     }

}
