package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.possible_unique_binary_tree;


public class BinaryTreeUniqueTreeCount {

    public static  int countTrees(int nodeCount){
        /*whenever the no of nodes is 1 there is just one possible tree- this is base case.*/
        if(nodeCount<=1)
            return 1;

        int sum=0;
        /*every node can be root node, hence we are traversing over each node*/
        for (int i = 1; i <= nodeCount; i++) {
            /*The nodes before it will on the LEFT*/
            int countLeftTrees=countTrees(i-1);
            /*The nodes after it on the RIGHT*/
            int countRightTrees=countTrees(nodeCount-i);
            /*multiplication needed to get all possible trees with this root*/
            sum=sum+(countLeftTrees*countRightTrees);
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(countTrees(4));
    }
}
