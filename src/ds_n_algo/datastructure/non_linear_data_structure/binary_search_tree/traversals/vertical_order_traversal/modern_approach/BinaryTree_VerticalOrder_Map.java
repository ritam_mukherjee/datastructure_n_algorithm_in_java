package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.traversals.vertical_order_traversal.modern_approach;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;
import java.util.stream.Collectors;

class Node {
    /*each node having a data*/
    int data;

    /*also two child nodes*/
    Node left;
    Node right;

    public Node(int data) {
        this.data = data;
    }

}

public class BinaryTree_VerticalOrder_Map {

    Node root;

    void getVerticalOrder(Node root, int hd, TreeMap<Integer, List<Integer>> m) {
        // Break point
        if (root == null)
            return;

        m.computeIfAbsent(hd, integer -> new LinkedList<>());

        m.compute(hd,(integer, integers) -> {
            integers.add(root.data);
            return m.put(integer, integers);
        });

        // Store nodes in left subtree
        getVerticalOrder(root.left, hd - 1, m);

        // Store nodes in right subtree
        getVerticalOrder(root.right, hd + 1, m);
    }

    // The main function to print vertical oder of a binary tree
    // with given root
    void printVerticalOrder(Node root) {
        // Create a map and store vertical oder in map using
        // function getVerticalOrder()
        TreeMap<Integer, List<Integer>> m = new TreeMap<>();
        int hd = 0;
        getVerticalOrder(root, hd, m);

        // Traverse the map and print nodes at every horigontal

        m.entrySet().stream().forEach(distance->{
            System.out.println("distance :"+distance.getKey()+"->"
                    +"values : "
                    +distance.getValue().stream().map(String::valueOf).collect(Collectors.joining(",")));
        });
    }

    // Driver program to test above functions
    public static void main(String[] args) {

        BinaryTree_VerticalOrder_Map tree = new BinaryTree_VerticalOrder_Map();

        // TO DO Auto-generated method stub
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);
        root.right.left.right = new Node(8);
        root.right.right.right = new Node(9);
        System.out.println("Vertical Order traversal is");

        tree.root = root;
        tree.printVerticalOrder(tree.root);
    }

    /**
     Source  :   GeekForGeek
     Link    :   https://www.geeksforgeeks.org/level-order-tree-traversal/
     */


}
