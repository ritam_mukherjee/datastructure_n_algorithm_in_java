package ds_n_algo.datastructure.non_linear_data_structure.binary_search_tree.utilities;

class Node<X extends Comparable> {
    /*It represents the value of node,which instantiate at the time of node creation*/
    private X item;
    /*All node having two child node LEFT & RIGHT*/
    private Node left;
    private Node right;

    /*Node also have a reference of PARENT node*/
    private Node parent;

    /*Constructor:
        1.Use to specify item/data at the time of creation
        2.Also use initialize all pointer's*/
    public Node(X item) {
        this.item = item;
        this.left = null;
        this.right = null;
        this.parent = null;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public X getItem() {
        return item;
    }

    public void setItem(X item) {
        this.item = item;
    }
}

class BinarySearchTree<X extends Comparable>{
    Node Root;

    public Node getRoot() {
        return Root;
    }

    public void setRoot(Node root) {
        Root = root;
    }

    /**
     * @apiNote longest path from given node to leaf node among it's children
     * @param node : determine the height of tree from given node
     * @return numeric value of height
     */
    public int getHeight(Node node) {
        if (node == null)
            return 0;
        if(node.getLeft()==null && node.getRight()==null)
            return 0;

       /*
       first two conditions can be change by this
       if (node == null)
            return -1;
       */
        else {
            int lheight = getHeight(node.getLeft());
            int rheight = getHeight(node.getRight());
            /* We ad 1 to to account for the current depth of the tree*/
            if (lheight > rheight)  /*we also can use:  Math.max(lheight,rheight)*/
                return lheight + 1;
            else
                return rheight + 1;
        }
    }

    public int getHeight() {
      return getHeight(this.getRoot());
    }

    /**
     * As Binary search tree having lif child value less than node, hence extreme left node holding minimum value
     * @param node
     * @return
     */
    public X minimumValue(Node<X> node){
        if(node==null)
            return null;
        if(node.getLeft()==null)
            return node.getItem();

        return (X) minimumValue(node.getLeft());

    }
    public X minimumValue(){
        return (X) minimumValue((this.getRoot()));
    }
}
public class BinaryTreeUtility {

    public static void main(String[] args) {
        BinarySearchTree<Integer> integerTree=new BinarySearchTree<>();

        Node root=new Node(6);
        root.setLeft(new Node(2));
        root.setRight( new Node(10));
        root.getLeft().setLeft(new Node(0));
        root.getLeft().setRight(new Node(4));
        root.getRight().setLeft(new Node(8));
        root.getRight().setRight(new Node(12));


        BinarySearchTree<String> stringTree=new BinarySearchTree<>();
        Node string_root=new Node("ritam");
        string_root.setLeft(new Node("poulami"));
        string_root.setRight( new Node("somdatta"));
        string_root.getLeft().setLeft(new Node("astha"));
        string_root.getLeft().setRight(new Node("ramisha"));
        string_root.getRight().setLeft(new Node("Shalini"));
        string_root.getRight().setRight(new Node("Tulika"));



        integerTree.setRoot(root);
        System.out.println("Height of The integer Tree"+integerTree.getHeight());

        System.out.println("Minimum Value of integer tree is:"+integerTree.minimumValue(root));

        stringTree.setRoot(string_root);
        System.out.println("Height of The String Tree"+stringTree.getHeight());

        System.out.println("Minimum Value of string tree is:"+stringTree.minimumValue(string_root));




    }
}
