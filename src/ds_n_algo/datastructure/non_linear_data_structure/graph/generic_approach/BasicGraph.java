package ds_n_algo.datastructure.non_linear_data_structure.graph.generic_approach;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
/*
class  Graph<X> {

    private int vertex;// no. of vertices
    private LinkedList<X> adjacencyList[];  // Array  of lists for Adjacency List Representation

    public Graph(int vertex) {
        this.vertex = vertex;

        adjacencyList = new LinkedList[vertex];
        for (int i = 0; i < vertex; ++i)
            adjacencyList[i] = new LinkedList();
    }


    *//**
     * Function to add an edge into the graph
     * @param   : which vertex element to add
     * @param
     *//*
    void addEdge(int vertex, X w) {
        adjacencyList[vertex].add(w);    // Add w to v's list.
    }

    public int getVertex() {
        return vertex;
    }

    public void setVertex(int vertex) {
        this.vertex = vertex;
    }

    public LinkedList<X>[] getAdjacencyList() {
        return adjacencyList;
    }

    public void setAdjacencyList(LinkedList<X>[] adjacencyList) {
        this.adjacencyList = adjacencyList;
    }
}

class GraphProcessing<X>{

    Graph<X> graph;

    void DFS()
    {
        // Mark all the vertices as not visited(set as
        // false by default in java)
        boolean visited[] = new boolean[graph.getVertex()];

        // Call the recursive helper function to print DFS traversal
        // starting from all vertices one by one
        for (int i=0; i<visited.length; ++i)
            if (visited[i] == false)
                DfsUtil(i, visited);
    }


    void DfsUtil(int v,boolean[] visited){

        // Mark the current node as visited and print it
        visited[v] = true;
        System.out.print(v+" ");

        // Recur for all the vertices adjacent to this vertex
        Iterator<X> i = graph.getAdjacencyList()[v].listIterator();
        while (i.hasNext())
        {
            X n = i.next();
            if (!visited[n])
                DfsUtil(n,visited); // call the utility method recursively
        }
    }
}*/


class Node<X>
{
    X data;
    boolean visited;
    List<Node> neighbours;

    Node(X data)
    {
        this.data=data;
        this.neighbours=new ArrayList<>();

    }
    public void addneighbours(Node neighbourNode)
    {
        this.neighbours.add(neighbourNode);
    }
    public List<Node> getNeighbours() {
        return neighbours;
    }
    public void setNeighbours(List<Node> neighbours) {
        this.neighbours = neighbours;
    }
}

class Graph{
    public  void dfs(Node node)
    {
        System.out.print(node.data + " ");
        List<Node> neighbours=node.getNeighbours();
        node.visited=true;
        for (int i = 0; i < neighbours.size(); i++) {
            Node n=neighbours.get(i);
            if(n!=null && !n.visited)
            {
                dfs(n);
            }
        }
    }

}

public class BasicGraph {
    public static void main(String[] args) {
        Node node40 =new Node(40);
        Node node10 =new Node(10);
        Node node20 =new Node(20);
        Node node30 =new Node(30);
        Node node60 =new Node(60);
        Node node50 =new Node(50);
        Node node70 =new Node(70);

        node40.addneighbours(node10);
        node40.addneighbours(node20);
        node10.addneighbours(node30);
        node20.addneighbours(node10);
        node20.addneighbours(node30);
        node20.addneighbours(node60);
        node20.addneighbours(node50);
        node30.addneighbours(node60);
        node60.addneighbours(node70);
        node50.addneighbours(node70);

        Graph g=new Graph();
        g.dfs(node40);
    }
}
