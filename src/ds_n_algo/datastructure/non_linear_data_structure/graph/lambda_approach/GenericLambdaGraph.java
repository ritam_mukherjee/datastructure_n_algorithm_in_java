package ds_n_algo.datastructure.non_linear_data_structure.graph.lambda_approach;

import java.util.LinkedList;
import java.util.function.Consumer;

public class GenericLambdaGraph {


    private int V;// no. of vertices
    private LinkedList<Integer> adj[];  // Array  of lists for Adjacency List Representation

    public GenericLambdaGraph(int v) {
        V = v;
        adj = new LinkedList[v];
        for (int i=0; i<v; ++i)
            adj[i] = new LinkedList();
    }
    //Function to add an edge into the graph
    void addEdge(int v, int w)
    {
        adj[v].add(w);    // Add w to v's list.
    }

    Consumer<Integer> DFS=(startingIndex)->
    {
        boolean visited[] = new boolean[V];
        DfsUtil(startingIndex,visited);
    };


    private void DfsUtil(int v,boolean[] visited){

        // Mark the current node as visited and print it
        visited[v] = true;
        System.out.print(v+" ");
        adj[v].stream().forEach(element->{
            if (!visited[element])
                DfsUtil(element,visited);
        });

    }


    // prints BFS traversal from a given source s
    void BFS(int s)
    {
        // Mark all the vertices as not visited(By default set as false)
        boolean visited[] = new boolean[V];

        // Create a queue for BFS
        LinkedList<Integer> queue = new LinkedList<Integer>();

        // Mark the current node as visited and enqueue it
        visited[s]=true;
        queue.add(s);

        while (queue.size() != 0)
        {
            // Dequeue a vertex from queue and print it
            s = queue.poll();
            System.out.print(s+" ");
            adj[s].stream().forEach(element->{
                if (!visited[element])
                {
                    visited[element] = true;
                    queue.add(element);
                }
            });
        }
    }
    public static void main(String args[])
    {
        GenericLambdaGraph g = new GenericLambdaGraph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        System.out.println("Following is Depth First Traversal");

        g.DFS.accept(2);

        System.out.println("\n----------------------------------------");
        System.out.println("Following is Breadth First Traversal "+
                "(starting from vertex 2)");

        g.BFS(2);

        System.out.println("\n----------------------------------------");
    }


}
