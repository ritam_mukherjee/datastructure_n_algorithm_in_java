package ds_n_algo.datastructure.linear_data_structure.arrays.progs.rotate_array;

import java.util.Arrays;

/**
 * problem:
 * reference :
 */
public class RotateOneByOneApproach {

    public void rotate(int[] nums, int k) {
        int n=nums.length;
        for (int i = 0; i <= k; i++)
            leftRotatebyOne(nums, n);
    }


    void leftRotatebyOne(int arr[], int n)
    {
        int i, temp;
        temp = arr[0];
        for (i = 0; i < n - 1; i++)
            arr[i] = arr[i + 1];
        arr[i] = temp;
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5,6,7};
        int k = 3;

        new RotateOneByOneApproach().rotate(nums,k);
        System.out.println(Arrays.toString(nums));
    }
}
