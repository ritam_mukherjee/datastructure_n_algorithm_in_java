package ds_n_algo.datastructure.linear_data_structure.arrays.progs.rotate_array;

import java.util.Arrays;


/**
 * Complex approac
 * complexity O(n)
 */
public class RotateArrayGCDApproach {

        public void rotate(int[] arr, int d) {
        int n=arr.length;

            d = d % n;
            int i, j, k, temp;
            int g_c_d = gcd(d, n);
            System.out.println(g_c_d);
            for (i = 0; i < g_c_d; i++) {
                /* move i-th values of blocks */
                temp = arr[i];
                j = i;
                while (true) {
                    k = j + d;
                    if (k >= n)
                        k = k - n;
                    if (k == i)
                        break;
                    arr[j] = arr[k];
                    j = k;
                }
                arr[j] = temp;
            }
        }

    /*Function to get gcd of a and b*/
    private static int gcd(int a, int b)
    {
        if (b == 0)
            return a;
        else
            return gcd(b, a % b);
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5,6,7};
        int k = 3;

        new RotateArrayGCDApproach().rotate(nums,k);
        System.out.println(Arrays.toString(nums));
    }
}
