package ds_n_algo.datastructure.linear_data_structure.arrays;

public class ArrayProg1 {
    public static void main(String[] args) {

        int[] arr={1,2,3,4,5};
        int n=arr.length-1;

        for (int i = 0; i < (1<<n); i++)
        {
            System.out.print("{ ");

            // Print current subset
            for (int j = 0; j < n; j++) {

                // (1<<j) is a number with jth bit 1
                // so when we 'and' them with the
                // subset number we get which numbers
                // are present in the subset and which
                // are not
                if ((i & (1 << j)) > 0)
                    System.out.print(arr[j] + " ");
            }
            System.out.println("}");
        }
    }
}
