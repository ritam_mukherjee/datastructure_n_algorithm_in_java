package ds_n_algo.datastructure.linear_data_structure.linkedlists.find_intersection;
class Node{

    /*the class having two property:
        a. val-> which represent the containing value of the element
        b. next-> reference of next element.*/
    int val;
    Node next;

    /*It should have one constructor so that at the time of Node creation providing value is mandatory*/
    public Node(int val) {
        this.val = val;
        next=null;
    }
}
public class LinkedListFindIntersection {
    static int findMergeNode(Node headA, Node headB) {
        Node currentA = headA;
        Node currentB = headB;

        //Do till the two nodes are the same
        while(currentA != currentB){
            //If you reached the end of one list start at the beginning of the other one
            //currentA
            if(currentA.next == null){
                currentA = headB;
            }else{
                currentA = currentA.next;
            }
            //currentB
            if(currentB.next == null){
                currentB = headA;
            }else{
                currentB = currentB.next;
            }
        }
        return currentA.val;

    }


    public static void main(String[] args) {
        Node aNode=new Node(2);
        Node aNode2=new Node(5);


        Node bNode=new Node(4);
        Node bNode2=new Node(3);

        Node cNode=new Node(8);
        Node cNode2=new Node(11);

        aNode.next=aNode2;
        aNode2.next=cNode;


        bNode.next=bNode2;
        bNode2.next=cNode;

        cNode.next=cNode2;
        LinkedListFindIntersection intersection=new LinkedListFindIntersection();
        System.out.println(LinkedListFindIntersection.findMergeNode(aNode,bNode));
    }
}
